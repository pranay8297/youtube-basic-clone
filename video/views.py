import json

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils import timezone

from video.models import Video, Comment
from video.forms import VideoForm, CommentForm

# Create your views here.
#create a view for the uplaoding video
#create a view for detail view for the video
#create a view for uploading a comment
#create a view to lsit all the videos

class VideoUpload(FormView):
    template_name = 'video/video_upload.html'
    form_class = VideoForm
    def form_valid(self, form):
        import pdb; pdb.set_trace()
        video = form.save(commit = False)
        video.user = self.request.user
        video.save()
        return redirect('home')
    
class HomeView(ListView):
    model = Video

class VideoDetailView(DetailView):
    model = Video
    template_name = 'video/video_detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['time'] = timezone.now()
        context['comment_form'] = CommentForm()
        # import pdb; pdb.set_trace()
        video_key = self.kwargs['pk']
        comments = Comment.objects.get_video_comments(pk = video_key)
        context['comments'] = comments
        return context

    def post(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        comment_text = self.request.POST.get('comment')
        vid = Video.objects.get(pk = self.kwargs['pk'])
        comment = Comment(user = self.request.user, video = vid, comment = comment_text)
        comment.save()
        response_data = {}
        response_data['result'] = 'successfully created the comment'
        response_data['comment_text'] = comment.comment
        response_data['author'] = self.request.user.username
        response_data['pk'] = comment.pk
        return HttpResponse(json.dumps(response_data),
                                content_type = 'application/json')
