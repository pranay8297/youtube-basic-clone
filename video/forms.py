from django import forms
from django.forms import ModelForm

from video.models import Video, Comment

class VideoForm(ModelForm):
    class Meta:
        model = Video
        fields = ('title',
                    'description',
                    'video_url')

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ('comment',)
        widgets = {
            'text': forms.TextInput(attrs={
                'id': 'post-text', 
                'required': True, 
                'placeholder': 'Say something...'
            }),
        }
