from django.db import models
from django.conf import settings


class CommentQuerySet(models.query.QuerySet):
    def get_video_comments(self, **kwargs):
        vid = Video.objects.get(pk = kwargs['pk'])
        comments = self.filter(video = vid)
        return comments

class Video(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            on_delete = models.CASCADE,
                            related_name = 'uploader')
    video_url = models.FileField(upload_to = 'videos/', null = True, verbose_name = '')
    title = models.CharField(max_length = 200)
    description = models.TextField(max_length = 600)

    def __str__(self):
        return self.heading

class Comment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            on_delete = models.CASCADE,
                            related_name = 'commenter')
    video = models.ForeignKey(Video, on_delete = models.CASCADE)
    comment = models.CharField(max_length = 400)
    objects = CommentQuerySet.as_manager()
    
    def __str__(self):
        return self.comment